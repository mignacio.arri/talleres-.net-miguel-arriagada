﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication9.Models;

namespace WebApplication9.Controllers
{
    public class SERVICIOSController : Controller
    {
        private testEntities db = new testEntities();

        // GET: SERVICIOS
        public ActionResult Index()
        {
            return View(db.SERVICIOS.ToList());
        }

        // GET: SERVICIOS/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SERVICIOS sERVICIOS = db.SERVICIOS.Find(id);
            if (sERVICIOS == null)
            {
                return HttpNotFound();
            }
            return View(sERVICIOS);
        }

        // GET: SERVICIOS/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SERVICIOS/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SERVI_ID,SERVI_DESCRIPCION,SERVI_VALOR")] SERVICIOS sERVICIOS)
        {
            if (ModelState.IsValid)
            {
                db.SERVICIOS.Add(sERVICIOS);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sERVICIOS);
        }

        // GET: SERVICIOS/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SERVICIOS sERVICIOS = db.SERVICIOS.Find(id);
            if (sERVICIOS == null)
            {
                return HttpNotFound();
            }
            return View(sERVICIOS);
        }

        // POST: SERVICIOS/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SERVI_ID,SERVI_DESCRIPCION,SERVI_VALOR")] SERVICIOS sERVICIOS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sERVICIOS).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sERVICIOS);
        }

        // GET: SERVICIOS/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SERVICIOS sERVICIOS = db.SERVICIOS.Find(id);
            if (sERVICIOS == null)
            {
                return HttpNotFound();
            }
            return View(sERVICIOS);
        }

        // POST: SERVICIOS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SERVICIOS sERVICIOS = db.SERVICIOS.Find(id);
            db.SERVICIOS.Remove(sERVICIOS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
